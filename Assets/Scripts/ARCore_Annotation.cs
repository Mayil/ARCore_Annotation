﻿namespace GoogleARCore.HelloAR
{
	using System.Collections;
	using System.Collections.Generic;
	using GoogleARCore;
	using UnityEngine;
	using UnityEngine.Rendering;
	using UnityEngine.UI;

	/// <summary>
	/// Controls the HelloAR example.
	/// </summary>
	public class ARCore_Annotation : MonoBehaviour
	{
		/// <summary>
		/// The first-person camera being used to render the passthrough camera image (i.e. AR background).
		/// </summary>
		/// 
		public static ARCore_Annotation instance;
		public Camera FirstPersonCamera;
		[SerializeField]
		GameObject Cube;
		[SerializeField]
		Text Anchor, Message;
		[SerializeField]
		GameObject Palette;

		//
		Vector3 mousePos;
		/// <summary>
		/// A prefab for tracking and visualizing detected planes.
		/// </summary>
		public GameObject TrackedPlanePrefab;

		/// <summary>
		/// A model to place when a raycast from a user touch hits a plane.
		/// </summary>
		/// A Quad is used as the plane will be generated based on this Prefab
		public GameObject AnchorPrefab;

		/// <summary>
		/// A gameobject parenting UI for displaying the "searching for planes" snackbar.
		/// </summary>
		public GameObject SearchingForPlaneUI;

		/// <summary>
		/// A list to hold new planes ARCore began tracking in the current frame. This object is used across
		/// the application to avoid per-frame allocations.
		/// </summary>
		private List<TrackedPlane> m_NewPlanes = new List<TrackedPlane>();

		/// <summary>
		/// A list to hold all planes ARCore is tracking in the current frame. This object is used across
		/// the application to avoid per-frame allocations.
		/// </summary>
		private List<TrackedPlane> m_AllPlanes = new List<TrackedPlane>();

		/// <summary>
		/// True if the app is in the process of quitting due to an ARCore connection error, otherwise false.
		/// </summary>
		private bool m_IsQuitting = false;

		Anchor anchor;
		Plane obj;
		public Color colorTrail = new Color(1, 1, 1, 1);
		Vector3 startPos, updatingPos;

		//for keeping track of how many touches
		int TouchCount = 0, tutCount = 0;
		int PlaneTouchCount = 0;
		GameObject go = null;
		GameObject AnchorObject = null;
		bool newAnchor = false;

		private void Start()
		{
			instance = this;
			obj = new Plane(FirstPersonCamera.transform.forward *= 1f, FirstPersonCamera.transform.position);
			Message.text = "Tap on plane";
			StartCoroutine(LateStart(5.0f));
			colorTrail = ColorPickerTester.instance.val;
		}

		IEnumerator LateStart(float wait)
		{
			yield return new WaitForSeconds(wait);
			Palette.SetActive(true);
		}


		/// <summary>
		/// The Unity Update() method.
		/// </summary>
		public void Update()
		{
			if (Input.GetKey(KeyCode.Escape))
			{
				Application.Quit();
			}

			_QuitOnConnectionErrors();

			// Check that motion tracking is tracking.
			if (Frame.TrackingState != TrackingState.Tracking)
			{
				const int lostTrackingSleepTimeout = 15;
				Screen.sleepTimeout = lostTrackingSleepTimeout;
				return;
			}

			Screen.sleepTimeout = SleepTimeout.NeverSleep;

			// Iterate over planes found in this frame and instantiate corresponding GameObjects to visualize them.
			Frame.GetPlanes(m_NewPlanes, TrackableQueryFilter.New);
			for (int i = 0; i < m_NewPlanes.Count; i++)
			{
				// Instantiate a plane visualization prefab and set it to track the new plane. The transform is set to
				// the origin with an identity rotation since the mesh for our prefab is updated in Unity World
				// coordinates.
				GameObject planeObject = Instantiate(TrackedPlanePrefab, Vector3.zero, Quaternion.identity,
				   transform);
				planeObject.GetComponent<TrackedPlaneVisualizer>().Initialize(m_NewPlanes[i]);
			}

			// Disable the snackbar UI when no planes are valid.
			Frame.GetPlanes(m_AllPlanes);
			bool showSearchingUI = true;
			for (int i = 0; i < m_AllPlanes.Count; i++)
			{
				if (m_AllPlanes[i].TrackingState == TrackingState.Tracking)
				{
					showSearchingUI = false;
					break;
				}
			}

			SearchingForPlaneUI.SetActive(showSearchingUI);

			// If the player has not touched the screen, we are done with this update.
			/*Touch touch;
			touch = Input.GetTouch(0);

			if (Input.touchCount < 1)
			{
				return;
			}

			// Raycast against the location the player touched the screen to search for planes.
			TrackableHit hit;
			//this is used so that new plane will be created without any error!
			


			TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinBounds | TrackableHitFlags.PlaneWithinPolygon;

			///<UserDefinedArea>
			///The Code that is related to touch and draw lines.
			///
			colorTrail = ColorPickerTester.instance.val;
			if ((TouchCount == 0) && (Session.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit)))
			{
				//have initialzied an empty object for using this object as an anchor and produce a plane
				AnchorObject = Instantiate(AnchorPrefab, hit.Pose.position, hit.Pose.rotation);

				// Create an anchor to allow ARCore to track the hitpoint as understanding of the physical
				// world evolves.
				anchor = hit.Trackable.CreateAnchor(hit.Pose);
				TouchCount = 1;
				Anchor.text = "Anchor Detected  "+Input.touchCount;

				// Andy should look at the camera but still be flush with the plane.
				//AnchorObject.transform.LookAt(FirstPersonCamera.transform);
				AnchorObject.transform.rotation = Quaternion.Euler(AnchorPrefab.transform.rotation.eulerAngles.x, AnchorObject.transform.rotation.eulerAngles.y, AnchorObject.transform.rotation.z);

				// Make Andy model a child of the anchor.
				AnchorObject.transform.parent = anchor.transform;
				
			}*/

			Ray mray = FirstPersonCamera.ScreenPointToRay(Input.mousePosition);
			RaycastHit hitPlane;
			//this condition for creatng the plane necessary for drawing object
			if (Physics.Raycast(mray, out hitPlane, 100f))
			{

			}
			switch (Input.GetTouch(0).phase)
			{

				case TouchPhase.Began:
					{
						if (TouchCount > 0)
						{
							TouchCount = 2;
							//obj = new Plane(FirstPersonCamera.transform.forward *= 1f, FirstPersonCamera.transform.position + new Vector3(AnchorObject.transform.position.x, AnchorObject.transform.position.y, AnchorObject.transform.position.z));
							//Ray mray = FirstPersonCamera.ScreenPointToRay(Input.mousePosition);
							//float rayDistance;
							//RaycastHit hitPlane;
							if (Physics.Raycast(mray, out hitPlane, 100f))
							{
								startPos = mray.GetPoint(hitPlane.distance);
							}
							/*if (obj.Raycast(mray, out rayDistance))
							{
								//Getting startPosition
								startPos = mray.GetPoint(rayDistance);
							}*/
							Anchor.text = "CreatedPlane  " + startPos;
							if (tutCount < 4)
								Message.text = "Draw Anywhere";
							if (AnchorObject)
							{
								//Ray mray = FirstPersonCamera.ScreenPointToRay(Input.mousePosition);
								//RaycastHit hitPlane;
								if (Physics.Raycast(mray, out hitPlane, 100f))
								{
									if (hitPlane.transform.gameObject.GetComponent<PlaneAnchoredOrNot>().nextPlane)
									{
										hitPlane.collider.enabled = false;
										Anchor.text = "ColliderDisabled  " + AnchorObject.GetComponent<MeshCollider>().enabled;
									}
								}
							}
						}
						break;
					}
				case TouchPhase.Moved:
					{
						if (hitPlane.transform.gameObject.GetComponent<PlaneAnchoredOrNot>().AnchoredOrNot)
						{
							if (TouchCount > 1)
							{
								//for creating the drawing object
								if (!go)
								{
									TouchCount += 1;
									//Ray mray = FirstPersonCamera.ScreenPointToRay(Input.mousePosition);
									//float rayDistance;
									/*if (obj.Raycast(mray, out rayDistance))
									{
										updatingPos = mray.GetPoint(rayDistance);
									}*/
									//RaycastHit hitPlane;
									if (Physics.Raycast(mray, out hitPlane, 100f))
									{
										updatingPos = mray.GetPoint(hitPlane.distance);

									}
									go = (GameObject)Instantiate(Cube, updatingPos, Quaternion.identity);
									go.transform.LookAt(FirstPersonCamera.transform);
									go.transform.rotation = Quaternion.Euler(0, go.transform.rotation.eulerAngles.y, go.transform.rotation.eulerAngles.z);
									go.GetComponent<TrailRenderer>().startColor = colorTrail;
									go.GetComponent<TrailRenderer>().endColor = colorTrail;

									//parenting the object,so that the object does not moves in ARCam
									go.transform.parent = AnchorObject.transform;
									Anchor.text = "CreatedObject  " + startPos + colorTrail;

								}
								//
								//if draw object is already created, then we can update its position in ARCam
								else
								{
									//Ray mray = FirstPersonCamera.ScreenPointToRay(Input.mousePosition);
									/*float rayDistance;
									if (obj.Raycast(mray, out rayDistance))
									{
										updatingPos = mray.GetPoint(rayDistance);

										//updating the position for trailrenderer
										go.transform.position = updatingPos;
									}*/
									//RaycastHit hitPlane;
									if (Physics.Raycast(mray, out hitPlane, 100f))
									{
										updatingPos = mray.GetPoint(hitPlane.distance);
										go.transform.position = updatingPos;
									}
									Anchor.text = "Moving Object  " + updatingPos + AnchorObject.GetComponents<MeshCollider>();
								}
							}
						}
						break;
					}
				case TouchPhase.Ended:
					{
						if (hitPlane.transform.gameObject.GetComponent<PlaneAnchoredOrNot>().AnchoredOrNot)
						{
							if (TouchCount > 2)
							{
								//for checking whether the line is drawn or not
								if (Vector3.Distance(go.transform.position, startPos) < 0.01)
								{
									Destroy(go);
									TouchCount = 1;

								}

								//
								//for creating another drawing for anchoring it in another position for depth analysing
								if (AnchorObject)
								{
									//Ray mray = FirstPersonCamera.ScreenPointToRay(Input.mousePosition);
									//RaycastHit hitPlane;
									if (Physics.Raycast(mray, out hitPlane, 100f) )
									{
										if(hitPlane.transform.gameObject.GetComponent<PlaneAnchoredOrNot>().nextPlane)
										{
											hitPlane.collider.enabled = false;
											Anchor.text = "ColliderDisabled  " + AnchorObject.GetComponent<MeshCollider>().enabled;
										}
									}
								}
								TouchCount = 1;
								if (AnchorObject)
									Anchor.text = "Collider " + AnchorObject.GetComponent<MeshCollider>().enabled;
								else
									Anchor.text = "AnchorObject is null";
								go = null;
								//AnchorObject = null;
								if (tutCount < 3)
								{
									Message.text = "Tap on the plane";
								}
								else
								{
									Message.text = "";
								}
								tutCount += 1;
							}
						}
					}
					break;
			}
		}


		
		public void PlaceAnchor()
		{
			// If the player has not touched the screen, we are done with this update.
			Touch touch;
			touch = Input.GetTouch(0);

			if (Input.touchCount < 1)
			{
				return;
			}

			// Raycast against the location the player touched the screen to search for planes.
			TrackableHit hit;
			//this is used so that new plane will be created without any error!

			TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinBounds | TrackableHitFlags.PlaneWithinPolygon;

			///<UserDefinedArea>
			///The Code that is related to touch and draw lines.
			///
			colorTrail = ColorPickerTester.instance.val;
			if ((Session.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit)))
			{
				if (AnchorObject)
				{
					if (AnchorObject.GetComponent<PlaneAnchoredOrNot>().PlaneTouchCount == 0)
						AnchorObject.GetComponent<PlaneAnchoredOrNot>().nextPlane = false;
					else
						AnchorObject.GetComponent<PlaneAnchoredOrNot>().nextPlane = true;
				}
				//have initialzied an empty object for using this object as an anchor and produce a plane
				AnchorObject = Instantiate(AnchorPrefab, hit.Pose.position, hit.Pose.rotation);

				// Create an anchor to allow ARCore to track the hitpoint as understanding of the physical
				// world evolves.
				anchor = hit.Trackable.CreateAnchor(hit.Pose);
				
				TouchCount = 1;
				AnchorObject.GetComponent<PlaneAnchoredOrNot>().PlaneTouchCount = 1;
				Anchor.text = "Anchor Detected  " + Input.touchCount;

				// Andy should look at the camera but still be flush with the plane.
				//AnchorObject.transform.LookAt(FirstPersonCamera.transform);
				AnchorObject.transform.rotation = Quaternion.Euler(AnchorPrefab.transform.rotation.eulerAngles.x, AnchorObject.transform.rotation.eulerAngles.y, AnchorObject.transform.rotation.z);

				// Make Andy model a child of the anchor.
				AnchorObject.transform.parent = anchor.transform;
				AnchorObject.GetComponent<PlaneAnchoredOrNot>().AnchoredOrNot = true;
				
			}
		}



		bool toggle = false;
		public Animator PalAnim;
		//For toggling color palette animation
		public void TogglePalette()
		{
			PalAnim.SetBool("Toggle", !toggle);
			//Palette.SetActive(!toggle);
			toggle = !toggle;
		}
		///
		///
		///</UserDefinedArea>


		/// <summary>
		/// Quit the application if there was a connection error for the ARCore session.
		/// </summary>
		private void _QuitOnConnectionErrors()
        {
            if (m_IsQuitting)
            {
                return;
            }

            // Quit if ARCore was unable to connect and give Unity some time for the toast to appear.
            if (Session.ConnectionState == SessionConnectionState.UserRejectedNeededPermission)
            {
                _ShowAndroidToastMessage("Camera permission is needed to run this application.");
                m_IsQuitting = true;
                Invoke("DoQuit", 0.5f);
            }
            else if (Session.ConnectionState == SessionConnectionState.ConnectToServiceFailed)
            {
                _ShowAndroidToastMessage("ARCore encountered a problem connecting.  Please start the app again.");
                m_IsQuitting = true;
                Invoke("DoQuit", 0.5f);
            }
        }

        /// <summary>
        /// Actually quit the application.
        /// </summary>
        private void DoQuit()
        {
            Application.Quit();
        }

        /// <summary>
        /// Show an Android toast message.
        /// </summary>
        /// <param name="message">Message string to show in the toast.</param>
        private void _ShowAndroidToastMessage(string message)
        {
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            if (unityActivity != null)
            {
                AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
                unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
                {
                    AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
                        message, 0);
                    toastObject.Call("show");
                }));
            }
        }

		
    }
}
