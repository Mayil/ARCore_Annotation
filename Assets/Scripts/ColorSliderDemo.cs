﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorSliderDemo : MonoBehaviour {

	[SerializeField]
	Slider Red, Blue, Green;
	[SerializeField]
	GameObject cube;
	// Use this for initialization
	void Start () {
		cube.GetComponent<Renderer>().material.SetColor("_Color",new Color32((byte)Red.value, (byte)Blue.value, (byte)Green.value,255));
	}
	
	// Update is called once per frame
	void Update () {
		cube.GetComponent<Renderer>().material.SetColor("_Color", new Color32((byte)Red.value, (byte)Blue.value, (byte)Green.value, 255));
	}
}
