﻿using UnityEngine;
using GoogleARCore.HelloAR;

/*

	"Did the swiping part in HelloARController Script"









*/



public class Swipe : MonoBehaviour
{

	public static Swipe instance;

	public GameObject trailPrefab;
	public Camera AR_CoreCam;
	
	//[SyncVar]
	public Vector3 startPos;
	//[SyncVar]
	public Vector3 updatingPos;

	Plane obj;
	GameObject thisTrail;

	void Start()
	{
		//for checking whether this is a localplayer or not is done in PlayerNetwork Script
		instance = this;
		obj = new Plane(AR_CoreCam.transform.forward *= 1f, AR_CoreCam.transform.position + (new Vector3(0, 0, 2f)));
	}


	// Update is called once per frame
	void Update()
	{
		//To check for the position of the mouse/finger at every frames
			Draw();
	}

	//Function for assigning startPosition and updatingPosition
	public void Draw()
	{
		//
		if ((Input.touchCount >1  && Input.GetTouch(0).phase == TouchPhase.Began) || Input.GetMouseButtonDown(0))
		{
			Debug.Log("Swiping");
			obj = new Plane(AR_CoreCam.transform.forward *= 1f, AR_CoreCam.transform.position + (new Vector3(0, 0, 2f)));

			Ray mray = AR_CoreCam.ScreenPointToRay(Input.mousePosition);
			float rayDistance;
			if (obj.Raycast(mray, out rayDistance))
			{
				//Getting startPosition
				startPos = mray.GetPoint(rayDistance);
			}
			//Instantiating the TrailRenderer prefab on the server and the startPosition is sent for client side object
			CmdInstan(startPos);
		}
		else if ((Input.touchCount > 1 && Input.GetTouch(0).phase == TouchPhase.Moved) || Input.GetMouseButton(0))
		{
			Ray mray = AR_CoreCam.ScreenPointToRay(Input.mousePosition);
			float rayDistance;
			if (obj.Raycast(mray, out rayDistance))
			{
				//Getting updatingPosition
				updatingPos = mray.GetPoint(rayDistance);
				thisTrail.transform.position = updatingPos;
			}
		}
		else if ((Input.touchCount > 1 && Input.GetTouch(0).phase == TouchPhase.Ended) || Input.GetMouseButtonUp(0))
		{
			//for checking whether the line is drawn or not
			if (Vector3.Distance(thisTrail.transform.position, startPos) < 0.1)
			{
				//NetworkServer.Destroy(thisTrail);
				Destroy(thisTrail);
			}
			//For client side only as we assign the networkspawned object to the client object(thisTrail) 
			//thisTrail = null;
		}
	}


	//[Command]
	public void CmdInstan(Vector3 start)
	{
		
		//Debug.Log("Command Executed");
		updatingPos = start;
		Debug.Log(updatingPos);
		thisTrail = (GameObject)Instantiate(trailPrefab, start, Quaternion.identity);
		//thisTrail.transform.parent = HelloARController.instance.anchor.transform;
		//Debug.Log("Server" + start);
		//for spawning the object with client authority so that the client 
		//can control an object which is being spawned in the server
		//NetworkServer.SpawnWithClientAuthority(thisTrail,connectionToClient);
		//RpcSpawn(thisTrail, start);

	}


	/*[ClientRpc]
	void RpcSpawn(GameObject Trail,Vector3 _startPos)
	{
		//assignment at the LocalClient Script
			thisTrail = Trail;
			Debug.Log("ClientRPC" + startPos);			
	}*/


}
	