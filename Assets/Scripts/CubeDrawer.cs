﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeDrawer : MonoBehaviour {

	[SerializeField]
	GameObject Cube,go;

	Vector3 mousePos,updatingPos;
	// Use this for initialization
	void Start () {
		
	}
	RaycastHit hitPlane;
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButton(0))
		{
			Ray mray = Camera.main.ScreenPointToRay(Input.mousePosition);
			
			if (Physics.Raycast(mray, out hitPlane, 100f))
			{
				updatingPos = mray.GetPoint(hitPlane.distance);
				Debug.Log(mray.GetPoint(hitPlane.distance));
			}
			go = (GameObject)Instantiate(Cube, updatingPos, Quaternion.identity);
		}
		if(Input.GetMouseButtonUp(0))
		{
			hitPlane.collider.enabled = false;
		}
	}
}
