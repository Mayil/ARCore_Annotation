﻿using UnityEngine.Networking;
using UnityEngine;

public class PlayerNetwork : NetworkBehaviour {
    [SerializeField]Behaviour[] componentsToDisable;
    

    void Start () {
        
        
        if (!isLocalPlayer)
        {          
            for (int i = 0; i < componentsToDisable.Length; i++)
            {
                componentsToDisable[i].enabled = false;
            }           
        }
        else
        {
            for (int i = 0; i < componentsToDisable.Length; i++)
            {
                componentsToDisable[i].enabled = true;
            }
        }
		
    }
    
	
  
	void Update () {
		
	}
}
