﻿using UnityEngine;
using System.Collections;
using GoogleARCore.HelloAR;

public class ColorPickerTester : MonoBehaviour 
{
	public static ColorPickerTester instance;
    [SerializeField]
    ColorPicker picker;
	public Color val=new Color(1,1,1,1);
	// Use this for initialization
	void Awake () 
    {
		instance = this;
		if (picker)
		{
			picker.onValueChanged.AddListener(color =>
			{
				
				val = color;
			});
			
		}
		
	}
	
	// Update is called once per frame
	void Update () {
		if (picker)
		{
			picker.onValueChanged.AddListener(color =>
			{
				
				val = color;
			});
			
		}

	}
}
